#!/usr/bin/env bash

patch_directory="../asunder_svn"

log_file="$patch_directory/svn_log.txt"


patch_file() {
    number=$1
    echo "$patch_directory/patch_$number.patch"
}

patch_date() {
    number=$1
    grep -r "^r$number" "$log_file" | awk -F'|' '{print $3}' | awk '{print $1" "$2}'
}

patch_message() {
    number=$1
    sed -n -e "/r$number/,/-----------------/ p" "$log_file" | sed -e '1d;2d;$d'
}


apply_all_patches() {
    first=$1
    last=$2

    for number in $(seq "$first" "$last"); do
        echo "Applying $number..."
        if ! git apply --exclude='releases/*' -p1 "$(patch_file "$number")"; then
            echo "Could not apply patch. Aborting."
            exit 1
        fi

        git commit -a \
            --author andrew \
            --date "$(patch_date "$number")" \
            --message "$(patch_message "$number")"

    done

}



apply_all_patches "$@"
