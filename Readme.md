# GrimRipper

[![pipeline status](https://gitlab.gnome.org/Salamandar/GrimRipper/badges/master/pipeline.svg)](https://gitlab.gnome.org/Salamandar/GrimRipper/-/commits/master)

GrimRipper is a graphical Audio CD ripper and encoder. You can use it to save tracks from an Audio CD as WAV, MP3, OGG, FLAC, and/or Wavpack.

This is a fork of Asunder (see <http://littlesvr.ca/asunder>).

## Features

* Supports WAV, MP3, Ogg Vorbis, FLAC, Opus, Wavpack, Musepack, AAC, and Monkey's audio files
* Uses CDDB to name and tag each track
* Can encode to multiple formats in one session
* Creates M3U playlists
* Allows for each track to be by a different artist
* Does not require a specific desktop environment

## Requirements

* Linux
* Meson build system >=0.49.0
* GTK+3 >=3.11.0
* Libcddb 0.9.5 or greater <https://libcddb.sourceforge.net>

To compile from source you may need the -dev or -devel versions of GTK and libcddb. On Ubuntu these packages are called libgtk2.0-dev and libcddb2-dev.

### Dependencies only required at runtime, as plug-ins

* Cdparanoia <https://www.xiph.org/paranoia>
* LAME (optional for MP3 support) <https://lame.sourceforge.net>
* Vorbis-Tools (optional for Ogg Vorbis support) <https://www.vorbis.com>
* Fraunhofer FDK AAC (optional for Aacsupport)
* FLAC (optional for FLAC support) <https://flac.sourceforge.net>
* Opus (optional for Opus support) <https://opus-codec.org>
* Wavpack (optional for wavpack support) <https://www.wavpack.com>
* mpcenc (optional for Musepack support) <https://www.musepack.net>
* mac (optional for Monkey's Audio support) <https://www.monkeysaudio.com>

## Compiling

First, configure a build directory (here `_build`) by running this in the source directory :

    meson _build --prefix=/usr --buildtype=debug -Db_lto=true

Then go to the build directory and build the code

    cd _build
    ninja

You can see all the available build options by running this :

    meson configure

And change the current options with this:

    meson configure -Db_lto=false

Install the compiled code with :

    ninja install

You can override the installation directory with :

    DESTDIR=$(pwd)/install ninja install

## Contact, bug reports

You can open issues or feature requests on <https://gitlab.gnome.org/Salamandar/GrimRipper>.

The original home of Asunder still exists here : <http://littlesvr.ca/contact.php>

## Credits

See the AUTHORS file or click About/Credits in GrimRipper for a list of contributors.

## License

The source code for this version of GrimRipper is distributed under the GNU General Public Licence version 2.
