# Finnish translation for GrimRipper.
# This file is distributed under the same license as the GrimRipper package.
# Eero Salokannel <eero@salokannel.net>, 2007 - 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: 2.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-03-14 13:22+0100\n"
"PO-Revision-Date: 2018-11-12 16:42+0200\n"
"Last-Translator: Jani Kinnunen <jani.kinnunen@wippies.fi>\n"
"Language-Team: Finnish\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural= n!=1;\n"
"X-Generator: Poedit 1.6.11\n"

#: src/main.c:131 src/interface.c:230
msgid "Rip"
msgstr "Rippaa"

#: src/main.c:141
msgid "Track"
msgstr "Raita"

#: src/main.c:149
msgid "Artist"
msgstr "Esittäjä"

#: src/main.c:157 src/main.c:165
msgid "Title"
msgstr "Kappale"

#: src/main.c:173
msgid "Time"
msgstr "Aika"

#: src/main.c:199
msgid ""
"'cdparanoia' was not found in your path. GrimRipper requires cdparanoia to rip "
"CDs."
msgstr ""
"'cdparanoia':a ei löytynyt. GrimRipper tarvitsee cdparanoia:n CD levyjen "
"rippaamiseen."

#: src/main.c:244 src/main.c:269 src/main.c:869
msgid "<b>Checking disc...</b>"
msgstr ""

#: src/main.c:538
msgid "<b>Getting disc info from the internet...</b>"
msgstr "<b>Haetaan levyn tietoja internetistä...</b>"

#: src/main.c:599
msgid "<b>Reading disc...</b>"
msgstr ""

#: src/callbacks.c:61 src/callbacks.c:319 src/callbacks.c:351
#: src/callbacks.c:362 src/callbacks.c:373 src/callbacks.c:385
#: src/interface.c:606 src/interface.c:685 src/interface.c:784
#: src/interface.c:913
#, c-format
msgid "%dKbps"
msgstr "%dKbps"

#: src/callbacks.c:750
msgid "No CD is inserted. Please insert a CD into the CD-ROM drive."
msgstr "Ei CD-levyä asemassa. Aseta levy asemaan."

#: src/callbacks.c:771 src/callbacks.c:799 src/callbacks.c:826
#: src/callbacks.c:853 src/callbacks.c:880 src/callbacks.c:908
#: src/callbacks.c:935 src/callbacks.c:962
#, c-format
msgid ""
"%s was not found in your path. GrimRipper requires it to create %s files. All %s "
"functionality is disabled."
msgstr ""
"%s ei löytynyt. GrimRipper tarvitsee sen tehdäkseen %s tiedostoja. Kaikki %s "
"toiminnot on poistettu käytöstä."

#: src/callbacks.c:1060
msgid "Select all for ripping"
msgstr "Valitse kaikki"

#: src/callbacks.c:1066
msgid "Deselect all for ripping"
msgstr "Poista valinnat"

#: src/callbacks.c:1072
msgid "Capitalize Artists & Titles"
msgstr ""

#: src/callbacks.c:1102
msgid "Swap Artist <=> Title"
msgstr ""

#: src/interface.c:85
msgid "CDDB Lookup"
msgstr "CDDB Haku"

#: src/interface.c:91 src/interface.c:372
msgid "Preferences"
msgstr "Asetukset"

#: src/interface.c:102
msgid "About"
msgstr ""

#: src/interface.c:135
msgid "Disc:"
msgstr "Levy:"

#: src/interface.c:139
msgid "Album Artist:"
msgstr "Levyn Artisti:"

#: src/interface.c:144
msgid "Album Title:"
msgstr "Levyn nimi:"

#: src/interface.c:149
msgid "Single Artist"
msgstr "Vain yksi artisti"

#: src/interface.c:157
msgid "First track number:"
msgstr ""

#: src/interface.c:170
msgid "Track number width in filename:"
msgstr ""

#: src/interface.c:197
msgid "Genre / Year:"
msgstr "Tyylilaji / Vuosi:"

#: src/interface.c:389 src/interface.c:395
msgid "Destination folder"
msgstr "Kohdehakemisto"

#: src/interface.c:399
msgid "Create M3U playlist"
msgstr "Tee M3U soittolista"

#: src/interface.c:407
msgid "CD-ROM device: "
msgstr "CD-ROM asema: "

#: src/interface.c:416
msgid ""
"Default: /dev/cdrom\n"
"Other example: /dev/hdc\n"
"Other example: /dev/sr0"
msgstr ""
"Oletus: /dev/cdrom\n"
"Malli 1: /dev/hdc\n"
"Malli 2: /dev/sr0"

#: src/interface.c:420
msgid "Eject disc when finished"
msgstr "Poista levy asemasta kun valmis"

#: src/interface.c:428
msgid "General"
msgstr "Yleistä"

#: src/interface.c:448
msgid ""
"%A - Artist\n"
"%L - Album\n"
"%N - Track number (2-digit)\n"
"%Y - Year (4-digit or \"0\")\n"
"%T - Song title"
msgstr ""
"%A - Artisti\n"
"%L - Levy\n"
"%N - Raidan numero (2-numeroinen)\n"
"%Y - Vuosi (4-numeroinen tai \"0\")\n"
"%T - Kappaleen nimi"

#: src/interface.c:453
#, c-format
msgid "%G - Genre"
msgstr "%G - Tyylilaji"

#: src/interface.c:469
msgid "Album directory: "
msgstr "Levyn kansio:"

#: src/interface.c:474 src/prefs.c:776
msgid "Playlist file: "
msgstr "Soittolista:"

#: src/interface.c:479 src/prefs.c:788 src/prefs.c:798
msgid "Music file: "
msgstr "Kappale:"

#: src/interface.c:490
msgid ""
"This is relative to the destination folder (from the General tab).\n"
"Can be blank.\n"
"Default: %A - %L\n"
"Other example: %A/%L"
msgstr ""
"Tämä riippuu kohdehakemistosta (Yleistä-välilehdellä).\n"
"Voi jättää tyhjäksi.\n"
"Oletus: %A - %L\n"
"Malli: %A/%L"

#: src/interface.c:500
msgid ""
"This will be stored in the album directory.\n"
"Can be blank.\n"
"Default: %A - %L"
msgstr ""
"Tämä tallennetaan levyn kansioon.\n"
"Voi jättää tyhjäksi.\n"
"Oletus: %A - %L"

#: src/interface.c:509
msgid ""
"This will be stored in the album directory.\n"
"Cannot be blank.\n"
"Default: %A - %T\n"
"Other example: %N - %T"
msgstr ""
"Tämä tallennetaan levyn kansioon.\n"
"Ei voi jättää tyhjäksi.\n"
"Oletus: %A - %T\n"
"Malli: %N - %T"

#: src/interface.c:514
msgid "Filename formats"
msgstr "Tiedostonimien muodostaminen"

#: src/interface.c:519
msgid "Allow changing first track number"
msgstr ""

#: src/interface.c:524
msgid "Filenames"
msgstr "Tiedostonimet"

#: src/interface.c:558
msgid "WAV (uncompressed)"
msgstr "WAV (pakkaamaton)"

#: src/interface.c:576
msgid "Variable bit rate (VBR)"
msgstr "Muuttuva bittivirta (VBR)"

#: src/interface.c:583
msgid "Better quality for the same size."
msgstr "Parempi laatu samaan tilaan."

#: src/interface.c:589 src/interface.c:668 src/interface.c:769
#: src/interface.c:851 src/interface.c:896
msgid "Bitrate"
msgstr "Bittinopeus"

#: src/interface.c:603 src/interface.c:682
msgid ""
"Higher bitrate is better quality but also bigger file. Most people use "
"192Kbps."
msgstr ""
"Korkeampi bittivirta tarkoittaa parempaa laatua, mutta myös isompaa "
"tiedostoa. Useimmiten käytetään 192Kbps."

#: src/interface.c:612
msgid "MP3 (lossy compression)"
msgstr "MP3 (häviöllinen pakkaus)"

#: src/interface.c:632
msgid "Quality"
msgstr "Laatu"

#: src/interface.c:642
msgid "Higher quality means bigger file. Default is 6."
msgstr "Korkeampi laatu tarkoittaa isompaa tiedostoa. Oletus on 6."

#: src/interface.c:644
msgid "OGG Vorbis (lossy compression)"
msgstr "OGG Vorbis (häviöllinen pakkaus)"

#: src/interface.c:691
#, fuzzy
msgid "AAC (lossy compression)"
msgstr "MP3 (häviöllinen pakkaus)"

#: src/interface.c:711 src/interface.c:817 src/interface.c:949
msgid "Compression level"
msgstr "Pakkaustaso"

#: src/interface.c:721 src/interface.c:961
msgid "This does not affect the quality. Higher number means smaller file."
msgstr ""
"Tämä ei vaikuta laatuun. Korkeampi luku tarkoittaa pienempää tiedostoa."

#: src/interface.c:723
msgid "FLAC (lossless compression)"
msgstr "FLAC (häviötön pakkaus)"

#: src/interface.c:741
msgid "More formats"
msgstr "Lisää pakkausmuotoja"

#: src/interface.c:782
msgid ""
"Higher bitrate is better quality but also bigger file. Most people use "
"160Kbps."
msgstr ""
"Korkeampi bittivirta tarkoittaa parempaa laatua, mutta myös isompaa "
"tiedostoa. Useimmiten käytetään 160Kbps."

#: src/interface.c:790
msgid "OPUS (lossy compression)"
msgstr "OPUS (häviöllinen pakkaus)"

#: src/interface.c:830
msgid ""
"This does not affect the quality. Higher number means smaller file. Default "
"is 1 (recommended)."
msgstr ""
"Tämä ei vaikuta laatuun. Korkeampi luku tarkoittaa pienempää tiedostoa. "
"Suositeltava oletusarvo on 1."

#: src/interface.c:836
msgid "Hybrid compression"
msgstr "Hybridipakkaus"

#: src/interface.c:845
msgid ""
"The format is lossy but a correction file is created for restoring the "
"lossless original."
msgstr ""
"Tiedostotyyppi on häviöllinen, mutta häviöttömän alkuperäistiedoston "
"palauttamiseksi tehdään korjaustiedosto."

#: src/interface.c:911
msgid "Higher bitrate is better quality but also bigger file."
msgstr ""
"Korkeampi bittivirta tarkoittaa parempaa laatua, mutta myös isompaa "
"tiedostoa."

#: src/interface.c:919
msgid "Musepack (lossy compression)"
msgstr "Musepack (häviöllinen pakkaus)"

#: src/interface.c:963
msgid "Monkey's Audio (lossless compression)"
msgstr "Monkey's Audio (häviötön pakkaus)"

#: src/interface.c:981
msgid "Encode"
msgstr "Koodaus"

#: src/interface.c:1015
msgid "Get disc info from the internet automatically"
msgstr "Hae levyn tiedot internetistä automaattisesti"

#: src/interface.c:1023 src/interface.c:1067
msgid "Server: "
msgstr "Palvelin:"

#: src/interface.c:1032
msgid "The CDDB server to get disc info from (default is gnudb.gnudb.org)"
msgstr "CDDB-palvelin, josta levyn tiedot haetaan (oletus on gnudb.gnudb.org)"

#: src/interface.c:1038 src/interface.c:1080
msgid "Port: "
msgstr "Portti:"

#: src/interface.c:1047
msgid "The CDDB server port (default is 8880)"
msgstr "CDDB-palvelimen portti (oletus on 8880)"

#: src/interface.c:1054
msgid "Use an HTTP proxy to connect to the internet"
msgstr "Käytä HTTP-välityspalvelinta internetyhteyden kanssa"

#: src/interface.c:1102
msgid "Artist/Title separator: "
msgstr ""

#: src/interface.c:1113
msgid "Log to /var/log/grimripper.log"
msgstr "Tallenna loki tiedostoon /var/log/grimripper.log"

#: src/interface.c:1118
msgid "Faster ripping (no error correction)"
msgstr "Nopeampi rippaus (ei virheenkorjausta)"

#: src/interface.c:1127
msgid "Advanced"
msgstr "Lisäasetukset"

#: src/interface.c:1183 src/interface.c:1219
msgid "Ripping"
msgstr "Rippaa"

#: src/interface.c:1213
msgid "Total progress"
msgstr "Kokonaisedistyminen"

#: src/interface.c:1225
msgid "Encoding"
msgstr "Koodaa"

#: src/interface.c:1865
#, c-format
msgid "%d file created successfully"
msgid_plural "%d files created successfully"
msgstr[0] "Luotiin %d tiedosto"
msgstr[1] "Luotiin %d tiedostoa"

#: src/interface.c:1874
#, c-format
msgid "There was an error creating %d file"
msgid_plural "There was an error creating %d files"
msgstr[0] "Esiintyi virhe luotaessa tiedostoa %d"
msgstr[1] "Esiintyi virhe luotaessa tiedostoja %d"

#: src/prefs.c:775 src/prefs.c:787
#, c-format
msgid "Invalid characters in the '%s' field"
msgstr "Virheellisiä merkkejä kentässä '%s'"

#: src/prefs.c:797
#, c-format
msgid "'%s' cannot be empty"
msgstr "'%s' ei voi olla tyhjä"

#: src/prefs.c:812
msgid "Invalid proxy port number"
msgstr "Virheellinen välityspalvelimen portin numero"

#: src/prefs.c:825
msgid "Invalid cddb server port number"
msgstr "Virheellinen cddb-palvelimen portin numero"

#: src/support.c:47
msgid "Overwrite?"
msgstr "Korvaa?"

#: src/support.c:60
#, c-format
msgid "The file '%s' already exists. Do you want to overwrite it?\n"
msgstr "Tiedosto '%s' on jo olemassa. Haluatko korvata sen?\n"

#: src/support.c:66
msgid "Remember the answer for _all the files made from this CD"
msgstr "Muista vastaus k_aikille tiedostoille tältä CD-levyltä"

#: src/threads.c:187
msgid ""
"No ripping/encoding method selected. Please enable one from the "
"'Preferences' menu."
msgstr "Pakkausmuoto valitsematta. Tee valinta Asetukset-valikosta."

#: src/threads.c:219
msgid ""
"No tracks were selected for ripping/encoding. Please select at least one "
"track."
msgstr "Ei valittuja raitoja. Valitse vähintään yksi raita."

#: src/threads.c:1299 src/threads.c:1301 src/threads.c:1305
msgid "Waiting..."
msgstr "Odottaa..."

#, fuzzy
#~ msgid ""
#~ "An application to save tracks from an Audio CD \n"
#~ "as WAV, MP3, OGG, FLAC, Wavpack, Opus, Musepack, Monkey's Audio, and/or "
#~ "AAC files."
#~ msgstr ""
#~ "Audio CD:n raitojen tallentamiseen tarkoitettu ohjelma.\n"
#~ "Tuetut pakkaustavat: WAV, MP3, OGG, FLAC, Opus, Wavpack, Musepack, "
#~ "Monkey's Audio ja  AAC."

#~ msgid "Genre"
#~ msgstr "Tyylilaji"

#~ msgid "Single Genre"
#~ msgstr "Vain yksi tyylilaji"

#~ msgid "Proprietary encoders"
#~ msgstr "Kaupalliset dekooderit"

#~ msgid "Higher quality means bigger file. Default is 60."
#~ msgstr "Korkeampi laatu tarkoittaa isompaa tiedostoa. Oletus on 60."

#~ msgid "AAC (lossy compression, Nero encoder)"
#~ msgstr "AAC (häviöllinen pakkaus, Nero Ltd.)"

#, fuzzy
#~ msgid ""
#~ "%s was not found in your path. GrimRipper requires it to create %s files.All "
#~ "%s functionality is disabled."
#~ msgstr ""
#~ "%s ei löytynyt. GrimRipper tarvitsee sen tehdäkseen %s tiedostoja. Kaikki %s "
#~ "toiminnot on poistettu käytöstä."

#, fuzzy
#~ msgid "Playlist file"
#~ msgstr "Soittolistan tiedosto: "

#, fuzzy
#~ msgid "Music file"
#~ msgstr "Musiikki tiedosto: "

#~ msgid "Create directory for each album"
#~ msgstr "Tee kansio jokaiselle levylle"

#~ msgid "These characters will be removed from all filenames."
#~ msgstr "Nämä merkit poistetaan kaikista tiedostonimistä."
